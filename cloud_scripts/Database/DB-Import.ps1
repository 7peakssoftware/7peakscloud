Param(
	[Parameter(Mandatory=$true)]
	[string]$file
)

# Prepare Dictionary
Get-Content "$file" | Foreach-Object{
    if ($_.StartsWith("#"))
    {
    }
	else
    {
		$var = $_.Split('|')
		New-Variable -Name $var[0] -Value $var[1] -Force
	}
}

# Login
if($OnAzureShell -eq $false) {
	Enable-AzureRmContextAutosave;
	Login-AzureRmAccount;
}

Write-Host "------";
Write-Host "Hi Master, I am here to serve you !";
Write-Host "";

# Select Subscription Id
Select-AzureRmSubscription -SubscriptionID $ImportSubscriptionId;

# Create working variables
$DbAdminPwd = $TargetDbAdminPwd | ConvertTo-SecureString -AsPlainText -Force;

Write-Host "------";
$UserChoice = Read-Host "Do you want drop existing database [$($TargetServerName)/$($TargetDBName)] ? (Y/N)"

if($UserChoice -eq 'Y') {
	Write-Host "";
	Write-Host "Operation can take a while. Go and take a cup of coffee.";

	# Remove the existing database
	Write-Host "Delete existing database";
	Remove-AzureRmSqlDatabase -ResourceGroupName $ImportResourceGroupName -ServerName $TargetServerName -DatabaseName $TargetDBName
	Write-Host "Delete existing database ... OK";

	# Create new data base
	Write-Host "Create database";
	New-AzureRmSqlDatabase -ResourceGroupName $ImportResourceGroupName -ServerName $TargetServerName -DatabaseName $TargetDBName
	Write-Host "Create database ... OK";
	Write-Host "";
} 
else 
{
	Write-Host "";
	Write-Host "Master, you choose to keep the existing database [$($TargetServerName)/$($TargetDBName)]";
	Write-Host "Your mercy will be appreciated, your name will branding in the mind of the people for a long time. Thanks.";
	Write-Host "";
}

# Import
$ImportResult = New-AzureRmSqlDatabaseImport -ResourceGroupName $ImportResourceGroupName -ServerName $TargetServerName -DatabaseName $TargetDBName -StorageKeyType "StorageAccessKey" -StorageKey $ImportStorageKey01 -StorageUri $ImportStoragePath -AdministratorLogin $TargetDbAdmin -AdministratorLoginPassword $DbAdminPwd -Edition Standard -ServiceObjectiveName S0 -DatabaseMaxSizeBytes 5000000;
Write-Host "Link:" $ImportResult.OperationStatusLink;
Write-Host "Status:" $ImportResult.Status;
Write-Host "Status Msg:"$ImportResult.StatusMessage;
Write-Host "ErrorMessage:" $ImportResult.ErrorMessage;

Write-Host "------";
Write-Host "That's all Folks !";