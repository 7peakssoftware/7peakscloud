<#
.SYNOPSIS

Export the database according the configuration properties file and the method ( Azure or Local )

.DESCRIPTION

This script will read the configuration properties file reading all the variables and try to export the database configured.

.PARAMETER file

The path to the configuration properties. Specifies the path of configuration file to export the database.

.PARAMETER command

Export, Import or Both. The default is both.

.EXAMPLE
   ./COMP-Export.ps1 -file <FilePath> -method Azure

.EXAMPLE
   ./COMP-Export.ps1 -file <FilePath> -method Local
#>
Param(
  [Parameter(Mandatory=$true)]
  [string]$file
)

# Prepare Dictionary
Get-Content "$file" | Foreach-Object{
    if ($_.StartsWith("#"))
    {
    }
	else
    {
		$var = $_.Split('|')
		New-Variable -Name $var[0] -Value $var[1] -Force
	}
}
function LocalSqlPackageExecute($Params){
	# Get SqlPackage installation
	$SqlPackagePath=& "C:\Windows\System32\where.exe" SqlPackage
	if($SqlPackagePath.Count -gt 1)
	{
		$SqlPackagePath=$SqlPackagePath[0]
	}

	Start-Process $SqlPackagePath -ArgumentList $Params -NoNewWindow -PassThru -Wait;
}

function ExportDatabase($FileName) {

  $StartTime = $(get-date -f yyyy-MM-dd_HH:mm);
  Write-Host "Start to Export Database" $StartTime;

  $ExportParams = @(
		"/a:Export"
    "/ssn:$SourceDBUrl"
    "/sdn:$SourceDBName"
    "/su:$SourceDbUserName"
    "/sp:$SourceDbUserPwd"
    "/tf:$FileName"
    "/p:Storage=File"
  );
  
  LocalSqlPackageExecute $ExportParams;

  $StopTime = $(get-date -f yyyy-MM-dd_HH:mm);
  Write-Host "Finish to Export Database" $StopTime;
}

function ImportDatabase($FileName) {

  $StartTime = $(get-date -f yyyy-MM-dd_HH:mm);
  Write-Host "Start to Import Database" $StartTime;

  $ImportParams = @(
		"/a:Import"
    "/tsn:$TargetDBUrl"
    "/tdn:$TargetDBName"
    "/tu:$TargetDbUserName"
    "/tp:$TargetDbUserPwd"
    "/sf:$FileName"
    "/p:DatabaseEdition=Standard"
    "/p:DatabaseServiceObjective=S0"
    "/p:Storage=File"
  );
  
  LocalSqlPackageExecute $ImportParams;

  $StopTime = $(get-date -f yyyy-MM-dd_HH:mm);
  Write-Host "Finish to Import Database" $StopTime;
}

Write-Host "Hi Master, I am here serve you ....";

$ExportFileName = "$ExportPath\$(get-date -f yyyy-MM-dd_HH-mm)_$SourceDBName.bacpac";
Write-Host "Export FileName is : " $ExportFileName;

# Export Database
ExportDatabase $ExportFileName;

Write-Host 'Please copy you target database to prevent any service disruption';
Write-Host '';
Write-Host 'You can use following SQL instruction';
Write-Host "------------------------";
Write-Host "USE master;";
Write-Host "ALTER DATABASE [$TargetDBName] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;";
Write-Host "ALTER DATABASE [$TargetDBName] MODIFY NAME = [$TargetDBName-COPY] ;";
Write-Host "ALTER DATABASE [$TargetDBName-COPY] SET MULTI_USER;";
Write-Host "------------------------";
Write-Host "OR";
Write-Host "------------------------";
Write-Host "ALTER DATABASE [$TargetDBName] MODIFY NAME = [$TargetDBName-COPY];";
Write-Host "------------------------";
Write-Host 'I wait here.';
Write-Host 'Press any key WHEN you are ready to continue...';
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

# Import Database
ImportDatabase $ExportFileName;

Write-Host "That's All Folks!";