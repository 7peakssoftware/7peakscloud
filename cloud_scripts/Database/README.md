# Azure Cloud DATABASE Script

## DB Export

Usage:

```
PS> .\DB-Export.ps1 -file <Properties file> -StorageKey01 <Storage Key #1> -OnAzureShell <True|False>
```

* <Properties file> contains all the variables declarations which is needed to execute the script
* <Storage Key #1> **not the #2**. It is a parameter due the Base64 format
* Login is required only if the scripts is launched by remote machine, otherwise launched by AzureShell no need to perform the login.


Example:
```
PS> .\DB-Export.ps1 -file DB-Export.example.ini -StorageKey01 "asdasdad......" -OnAzureShell 0
```

## DB Import

Usage:

```
PS> .\DB-Import.ps1 -file <Properties file> -StorageKey01 <Storage Key #1> -FileStoragePath <URI of BACPAC file on Storage> -OnAzureShell <True|False>
```

* <Properties file> contains all the variables declarations which is needed to execute the script
* <Storage Key #1> **not the #2**. It is a parameter due the Base64 format
* <URI of BACPAC file on Storage> it is the URI of the BACPAC file saved by the Export script.
* Login is required only if the scripts is launched by remote machine, otherwise launched by AzureShell no need to perform the login.


Example:
```
PS> .\DB-Import.ps1 -file DB-Import.example.ini -StorageKey01 "asdasdad......"  -FileStoragePath "https://irpccompdev.blob.core.windows.net/bacpac/database_2019-03-04_12:54.bacpac"  -OnAzureShell 0
```


## DAP Project customization

DAP project requries a customization due the export of the 2 databases, one for the API and one for SI application.

Export
```
PS> .\DAP-Export.ps1 -file DAP-Export.DEV.ini -StorageUri "https://irpcdashboarddev.blob.core.windows.net/" -StorageKey01 "Key01..." -OnAzureShell 0
```

Import
```
PS> .\DAP-Import.ps1 -file DAP-Imoprt.DEV.ini 
						-DAPFileStoragePath "https://irpcdashboarddev.blob.core.windows.net/bacpac/DAP-database-2019-03-04_11:30.bacpac"
						-SIFileStoragePath "https://irpcdashboarddev.blob.core.windows.net/bacpac/SI-database-2019-03-04_11:30.bacpac"
						-StorageKey01 "Key01..."  -OnAzureShell 0
```

## Use SQL PACKAGE

The above scripts use Azure Cloud as main resource. There is other ways to perform Export and Import of MS SQL Database.

Install the **SqlPackage** package, [SqlPackage 2017](https://docs.microsoft.com/en-us/sql/tools/sqlpackage-download?view=sql-server-2017)

The full reference for the command is [here](https://docs.microsoft.com/en-us/sql/tools/sqlpackage?view=sql-server-2017#command-line-syntax).

### Export

Export command line general description:

```
SqlPackage /a:Export /ssn:tcp:<ServerName>.database.windows.net,1433 /sdn:<DatabaseName> /su:<UserName> /sp:<Password> /tf:<TargetFile> /p:Storage=File
```

**Examples**
```
SqlPackage /a:Export /ssn:tcp:irpc-dev.database.windows.net,1433 /sdn:dashboard-dip-dev /su:irpc-dev-admin /sp:IDontRememberIt /tf:D:\SQL_BACKUPS\dip-dev_20190305-1133.bacpac /p:Storage=File
SqlPackage /a:Export /ssn:"(localdb)\mssqllocaldb" /sdn:myLocalDB /tf:D:\SQL_BACKUPS\MyLocalDB.bacpac /p:Storage=File
```

Adding `/p:TableData=[schema].[tableName]` can specify to export only the declared tables.

```
 /p:TableData=dbo.Benchmarks /p:TableData=dbo.MasterCrudeBenchmarks ....
```

### Import

Import requries always an empty database.

```
SqlPackage /a:Import /tsn:tcp:<ServerName>.database.windows.net,1433 /tdn:<TargetDatabaseName> /tu:<UserName> /tp:<Password> /sf:<Path to bacpac file> /p:DatabaseEdition=Standard /p:DatabaseServiceObjective=P4 /p:Storage=File
```

**Examples**
```
SqlPackage /a:Import /tsn:tcp:irpc-dev.database.windows.net,1433 /tdn:dip-2-dev /tu:irpc-dev-admin /tp:IDontRememberIt /sf:D:\SQL_BACKUPS\dip-dev_20190305-1133.bacpac /p:DatabaseEdition=Standard /p:DatabaseServiceObjective=P4 /p:Storage=File
SqlPackage /a:Import /tsn:"(localdb)\mssqllocaldb" /tdn:dashboard-dip-dev /sf:D:\TEMP_VOL\dip-dev.bacpac
```






