Param(
	[Parameter(Mandatory=$true)]
	[string]$file
)

# Prepare Dictionary
Get-Content "$file" | Foreach-Object{
    if ($_.StartsWith("#"))
    {
    }
	else
    {
		$var = $_.Split('|')
		New-Variable -Name $var[0] -Value $var[1] -Force
	}
}

# Login
if($OnAzureShell -eq $false) {
	Enable-AzureRmContextAutosave -Scope CurrentUser;
	Login-AzureRmAccount;
}

# Select Subscription Id
Select-AzureRmSubscription -SubscriptionID $ExportSubscriptionId;

# Current time and date for file name
$Date = $(get-date -f yyyy-MM-dd_HH:mm);

# Create working variables
$DbAdminPwd = $SourceDbAdminPwd | ConvertTo-SecureString -AsPlainText -Force;

# Export
$ExportResult = New-AzureRmSqlDatabaseExport -ResourceGroupName $ExportResourceGroupName -ServerName $SourceServerName -DatabaseName $SourceDBName -StorageKeyType "StorageAccessKey" -StorageKey $ExportStorageKey01 -StorageUri "$ExportStoragePath/export_$($Date)_$($SourceDBName).bacpac" -AdministratorLogin $SourceDbAdmin -AdministratorLoginPassword $DbAdminPwd;
Write-Host "Link:" $ExportResult.OperationStatusLink;
Write-Host "Status:" $ExportResult.Status;
Write-Host "Status Msg:"$ExportResult.StatusMessage;
Write-Host "ErrorMessage:" $ExportResult.ErrorMessage;



