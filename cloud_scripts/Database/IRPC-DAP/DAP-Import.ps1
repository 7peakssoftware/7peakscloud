Param(
	[Parameter(Mandatory=$true)]
	[string]$file,

	[Parameter(Mandatory=$true)]
	[string]$DAPFileName,

	[Parameter(Mandatory=$true)]
	[string]$SIFileName
)

# Prepare Dictionary
Get-Content "$file" | Foreach-Object{
    if ($_.StartsWith("#"))
    {
        
    }
	else
    {
        $var = $_.Split('|')
		New-Variable -Name $var[0] -Value $var[1] -Force
	}
}

# Login
if($OnAzureShell -eq "0") {
	Login-AzureRmAccount;
}
Write-Host $SubscriptionId;

# Select Subscription Id
Select-AzureRmSubscription -SubscriptionID $SubscriptionId;

# Create working variables
$DbAdminPwd = $TargetAdminPwd | ConvertTo-SecureString -AsPlainText -Force;

#Rename the existing database
#Set-AzureSqlDatabase -ServerName $TargetServerName -DatabaseName  $TargetDBNameDAP -NewDatabaseName $TargetDBNameDAP_$Date
#Set-AzureSqlDatabase -ServerName $TargetServerName -DatabaseName  $TargetDBNameSI -NewDatabaseName $TargetDBNameSI_$Date

Remove-AzureRmSqlDatabase -ResourceGroupName $ResourceGroupName -ServerName $TargetServerName -DatabaseName $TargetDBNameDAP
Remove-AzureRmSqlDatabase -ResourceGroupName $ResourceGroupName -ServerName $TargetServerName -DatabaseName $TargetDBNameSI

#Create new data base
New-AzureRmSqlDatabase -ResourceGroupName $ResourceGroupName -ServerName $TargetServerName -DatabaseName $TargetDBNameDAP
New-AzureRmSqlDatabase -ResourceGroupName $ResourceGroupName -ServerName $TargetServerName -DatabaseName $TargetDBNameSI


# Import
$DAPImportResult = New-AzureRmSqlDatabaseImport -ResourceGroupName $ResourceGroupName -ServerName $TargetServerName -DatabaseName $TargetDBNameDAP -StorageKeyType "StorageAccessKey" -StorageKey $StorageKey01 -StorageUri $DAPFileStoragePath/$DAPFileName -AdministratorLogin $TargetDbAdmin -AdministratorLoginPassword $DbAdminPwd -Edition Standard -ServiceObjectiveName S0 -DatabaseMaxSizeBytes 5000000;
Write-Host "Link:" $DAPImportResult.OperationStatusLink;
Write-Host "Status:" $DAPImportResult.Status;
Write-Host "Status Msg:"$DAPImportResult.StatusMessage;
Write-Host "ErrorMessage:" $DAPImportResult.ErrorMessage;

$SIImportResult = New-AzureRmSqlDatabaseImport -ResourceGroupName $ResourceGroupName -ServerName $TargetServerName -DatabaseName $TargetDBNameSI -StorageKeyType "StorageAccessKey" -StorageKey $StorageKey01 -StorageUri $SIFileStoragePath/$SIFileName -AdministratorLogin $TargetDbAdmin -AdministratorLoginPassword $DbAdminPwd -Edition Standard -ServiceObjectiveName S0 -DatabaseMaxSizeBytes 5000000;
Write-Host "Link:" $SIImportResult.OperationStatusLink;
Write-Host "Status:" $SIImportResult.Status;
Write-Host "Status Msg:"$SIImportResult.StatusMessage;
Write-Host "ErrorMessage:" $SIImportResult.ErrorMessage;



