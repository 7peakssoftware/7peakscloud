

#Step1: To export both Dashboard & SI of 7Peaks
.\DAP-Export.ps1 -file DAP-Export.7PeaksProd.ini 

#Check the dashboard storage account sqlbackups container for bacpac file

#Step2: To export both Dahboard & SI of IRPC
.\DAP-Export.ps1 -file DAP-Export.IRPCProd.ini 

#Check the dashboard storage account sqlbackups container for bacpac file


From 7Peaks to IRPC
===================
Step3: To import from 7peaks to IRPC
.\DAP-Import.ps1 -file DAP-Import.IRPCProd.ini 
                        -DAPFileName "Get the latest bacpac file from storage account eg.DAP-database-2019-03-04_11:30.bacpac"
                        -SIFileName "Get the latest bacpac file from storage account eg.SI-database-2019-03-04_11:30.bacpac"

From IRPC to 7Peaks
===================

Step3: To import from IRPC to 7Peaks
.\DAP-Import.ps1 -file DAP-Import.7PeaksProd.ini 
                        -DAPFileName "Get the latest bacpac file from storage account  eg.DAP-database-2019-03-04_11:30.bacpac"
                        -SIFileName "Get the latest bacpac file from storage account eg.SI-database-2019-03-04_11:30.bacpac"
                        