Param(
	[Parameter(Mandatory=$true)]
	[string]$file	
)

# Prepare Dictionary
Get-Content "$file" | Foreach-Object{
    if ($_.StartsWith("#"))
    {
    }
	else
    {
		$var = $_.Split('|')
		New-Variable -Name $var[0] -Value $var[1] -Force
	}
}

# Login
if($OnAzureShell -eq $false) {
	Login-AzureRmAccount;
}

# Select Subscription Id
Select-AzureRmSubscription -SubscriptionID $SubscriptionId;

# Current time and date for file name
$Date = $(get-date -f yyyy-MM-dd_HHmm);


# Create working variables
$DbAdminPwd01 = $SourceDbAdminPwd | ConvertTo-SecureString -AsPlainText -Force;
#$DbAdminPwd01 =  ConvertTo-SecureString "T2N0b2JlcjIwMTg=" -AsPlainText -Force;
#$DbAdminPwd01 =  ConvertTo-SecureString "D@taba5e" -AsPlainText -Force;

# Export
$DAPExportResult = New-AzureRmSqlDatabaseExport -ResourceGroupName $ResourceGroupName -ServerName $SourceServerName -DatabaseName $SourceDBNameDAP -StorageKeyType "StorageAccessKey" -StorageKey $StorageKey01 -StorageUri "$StorageUri/DAP_database_$Date.bacpac" -AdministratorLogin $SourceDbAdmin -AdministratorLoginPassword $DbAdminPwd01;
Write-Host "Link:" $DAPExportResult.OperationStatusLink;
Write-Host "Status:" $DAPExportResult.Status;
Write-Host "Status Msg:"$DAPExportResult.StatusMessage;
Write-Host "ErrorMessage:" $DAPExportResult.ErrorMessage;

$SIExportResult = New-AzureRmSqlDatabaseExport -ResourceGroupName $ResourceGroupName -ServerName $SourceServerName -DatabaseName $SourceDBNameSI -StorageKeyType "StorageAccessKey" -StorageKey $StorageKey01 -StorageUri "$StorageUri/SI_database_$Date.bacpac" -AdministratorLogin $SourceDbAdmin -AdministratorLoginPassword $DbAdminPwd01;
Write-Host "Link:" $SIExportResult.OperationStatusLink;
Write-Host "Status:" $SIExportResult.Status;
Write-Host "Status Msg:"$SIExportResult.StatusMessage;
Write-Host "ErrorMessage:" $SIExportResult.ErrorMessage;




