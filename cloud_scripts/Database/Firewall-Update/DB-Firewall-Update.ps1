﻿<#
.SYNOPSIS

Update the Firewall Rule of Azure SqlServer with current host Public IP.

.DESCRIPTION

Update the Firewall Rule of Azure SqlServer with current host Public IP.

.PARAMETER file

The path to the configuration properties.

.PARAMETER NewIP

The new IP (at now, 2019-06-20 Azure supports only IPv4)

.EXAMPLE
   ./DB-Firewall-Update.ps1 -file <FilePath> -NewIp <Public IPv4>

#>
Param(
	[Parameter(Mandatory=$true)]
	[string]$file,

    [Parameter(Mandatory=$true)]
	[string]$NewIP
)

# Prepare Dictionary
$parameters = Get-Content "$file" | ConvertFrom-Json

# Login
Login-AzureRmAccount;

foreach ($current in $parameters) {
    Write-Output "Switch subscription To: $($current.SubscriptionId)";
    Set-AzureRmContext -SubscriptionId $current.SubscriptionId;

    # Get SQL Firewall Rule
    $rule = Get-AzureRmSqlServerFirewallRule -ResourceGroupName $current.ResourceGroupName -ServerName $current.SqlServerName -FirewallRuleName $current.FWRuleName;

    # check
    if($rule) 
    {
        Write-Output "Update existing rule";

        Set-AzureRmSqlServerFirewallRule -ResourceGroupName $current.ResourceGroupName `
            -ServerName $current.SqlServerName `
            -FirewallRuleName $current.FWRuleName `
            -StartIpAddress $NewIP -EndIpAddress $NewIP;

        Write-Output "Done.";
    }
    else 
    {
        Write-Output "Rule not exists";
    }
}
