Param(
  [Parameter(Mandatory=$true)]
  [string]$file,
  
  [Parameter(Mandatory=$false)]
  [bool]$ForceCreate = $false
)

# Prepare Dictionary
Get-Content "$file" | Foreach-Object{
    if ($_.StartsWith("#"))
    {
    }
	else
    {
		$var = $_.Split(':')
		New-Variable -Name $var[0] -Value $var[1] -Force
	}
}

# LogIn
Write-Information "Logging in...";
Login-AzureRmAccount;

# select subscription
Write-Information "Selecting subscription '$SubscriptionId'";
Select-AzureRmSubscription -SubscriptionID $SubscriptionId;

$AppService = Get-AzureRmWebApp -ResourceGroupName $ResourceGroupName -Name $WebAppName;

# App Service not exist
if (!$AppService) {
    Write-Information "App Service $WebAppName NOT found.";
}  
# App Service exists
else {
    Write-Information "App Service $WebAppName found.";

    # HashTable
    $Properties = @{};

    # Copy existing variables
    foreach($item in $AppService.SiteConfig.AppSettings)
    {
        $Properties[$item.Name] = $item.Value;
    }

    $Properties.Add('ASPNETCORE_ENVIRONMENT', $WebAppEnv);
    $Properties.Add('IciApi:IcisBaseUri', $IcisUrl);
    $Properties.Add('IciApi:IcisLogin', $IcisLogin);
    $Properties.Add('IciApi:IcisPassword', $IcisPasswd);
    $Properties.Add('LoadIRPCData', $False);
    
    # Connection Strings ( go in override )
    $ConnStrings = @{
        CompDefaultConnection = @{ Type="SQLAzure"; Value=$ConnStringSQL }
        CompStorageConnection = @{ Type="Custom"; Value=$ConnStringStorage } 
    };

    $AppService = Set-AzureRmWebApp -ResourceGroupName $AppService.ResourceGroup  -Name $AppService.Name -AppSettings $Properties -ConnectionStrings $ConnStrings;

    # Print all AppSettings
    foreach($item in $AppService.SiteConfig.AppSettings)
    {
        Write-Information $item.Name ":" $item.Value;
    }

    # Print all Connection string
    foreach($item in $AppService.SiteConfig.ConnectionStrings)
    {
        Write-Information $item.Name ":" $item.ConnectionString ":" $item.Type;
    }

    # Restart ?????
}

exit 0;