Param(
  [Parameter(Mandatory=$true)]
  [string]$file,
  
  [Parameter(Mandatory=$false)]
  [string]$command
)

# Prepare Dictionary
Get-Content "$file" | Foreach-Object{
    if ($_.StartsWith("#"))
    {
    }
	else
    {
		$var = $_.Split('=')
		New-Variable -Name $var[0] -Value $var[1] -Force
	}
}

Write-Host "Logging in...";
Login-AzureRmAccount;

# select subscription
Write-Host "Selecting subscription '$subscriptionId'";
Select-AzureRmSubscription -SubscriptionID $subscriptionId;

if($command -eq 'stop') {
	# Stop Web App
	Write-Host "Stop '$webAppName' into '$resourceGroup'";
	Stop-AzureRmWebApp -ResourceGroupName $resourceGroup -Name $webAppName;
} 
elseif($command -eq 'start') {
	# Start Web App
	Write-Host "Start '$webAppName' into '$resourceGroup'";
	Start-AzureRmWebApp -ResourceGroupName $resourceGroup -Name $webAppName;
}
else {
	Write-Host "Default command is RESTART";
	
	Write-Host "Stop '$webAppName' into '$resourceGroup'";
	Stop-AzureRmWebApp -ResourceGroupName $resourceGroup -Name $webAppName;
	
	# Wait 10 seconds
	Start-Sleep -s 10
	
	Write-Host "Start '$webAppName' into '$resourceGroup'";
	Start-AzureRmWebApp -ResourceGroupName $resourceGroup -Name $webAppName;
}
