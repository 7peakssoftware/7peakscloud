FROM debian:stable

# set-up environment
RUN apt-get update && \
 apt-get install -y vim apt-utils ca-certificates curl apt-transport-https lsb-release gnupg

# Install Azure Cli
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

CMD