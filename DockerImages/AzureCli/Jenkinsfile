#!groovy

node('master') {

	try {
		properties([buildDiscarder(logRotator(numToKeepStr: '2'))])

		stage('Checkout') {
		    checkout scm
		}
		
        stage('Clean Remote images') {
            milestone()

            docker.withRegistry('https://7peaksci.azurecr.io', 'docker-build-server') {
                withCredentials([usernamePassword(credentialsId: 'jenkins-service-principal-credential', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {

                    // Usage of .inside() is afftected buy a bug. 
                    // Please keep this only other Jenkins tasks aren't running.
                    docker.image('7peaksci/azure-cli:latest').inside("-u root -v /var/lib/jenkins/caches:/var/lib/jenkins/caches"){
                        sh """
                            az login --service-principal -u ${USERNAME} -p ${PASSWORD} --tenant 7peakssoftware.com && \
                            az acr repository delete -y --name 7peaksci --repository 7peaksci/azure-cli
                        """
                    }
                }
            }
        }

		stage('Clean Local images') {
            milestone()
		    sh """ 
		  	    docker images | grep 'azure-cli'  | awk '{print \$3}' | xargs -r docker rmi -f 
		    """
		}

		stage('Build and Push Image'){
			milestone()

			docker.withRegistry('https://7peaksci.azurecr.io', 'docker-build-server') {
				docker
					.build("7peaksci/azure-cli", "-f ${WORKSPACE}/DockerImages/AzureCli/AzureCli.Dockerfile .")
					.push()
			}
		}

		currentBuild.result = 'SUCCESS'

	} catch(e) {
		throw e

	} finally {
		// check stattus
		def currentResult = currentBuild.result == 'SUCCESS' ? 'success' : 'failure'

		// send message to slack
		withCredentials([string(credentialsId: 'slack-giorgio-token', variable: 'TOKEN')]) {
			docker.image('mikewright/slack-client:latest')
			    .run('-e SLACK_TOKEN=$TOKEN -e SLACK_CHANNEL=#devops_7peaks_backend', 
			        "\"${env.JOB_NAME} - Build#${env.BUILD_NUMBER} - Status: ${currentResult}\n ${env.BUILD_URL}\"")
		}
	}
}
