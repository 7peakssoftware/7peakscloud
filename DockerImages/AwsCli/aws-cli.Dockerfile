FROM debian:stable

# install package
RUN apt-get update && \
 apt-get upgrade -y && \
 apt-get install -y apt-utils zip unzip ca-certificates less vim groff-base && \
 apt-get install -y awscli

# folder for configuration files
RUN mkdir /root/.aws

# move the workdir inside a proper place
WORKDIR /workspace_aws