FROM debian:stable

# install package
RUN apt-get update && \
 apt-get upgrade -y && \
 apt-get install -y apt-utils curl zip unzip ca-certificates less vim groff-base gnupg gnupg2 software-properties-common

# install Terraform
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
  apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
  apt-get update && apt-get install -y terraform git

# install the AWS-CLI v2
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
 unzip awscliv2.zip && \ 
 ./aws/install

# folder for configuration files
RUN mkdir /root/.aws

# move the workdir inside a proper place
WORKDIR /workspace_aws