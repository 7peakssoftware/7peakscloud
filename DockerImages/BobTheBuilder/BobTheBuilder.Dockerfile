FROM debian:stable

ARG PackageName
ARG RepositoryUrl=https://7peaks-ci-proget.azurewebsites.net/nuget/Nuget/
ARG VersionType=PATCH
ARG OutputFile=version.txt

# set-up environment
RUN apt-get update \
 && apt-get install -y apt-utils apt-transport-https curl wget gpg xmlstarlet \ 
 && apt-get upgrade -y \
 && apt-get install -y nuget 

# working directory
WORKDIR /home/bobTheBuilder/

COPY ./DockerImages/BobTheBuilder/increase_version.sh .

CMD