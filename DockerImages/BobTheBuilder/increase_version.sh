#!/bin/bash

package_name=$1
repository_url=$2
version_type=$3
output_file=$4

echo "Hi Master, I am here to serve you !"
echo
echo "Package Name   : $package_name"
echo "Repository Url : $repository_url"
echo "Version type   : $version_type"
echo "Output File    : $output_file"
echo

#
# Read Nuget version
#
nuget_read_version() {
	local url="$2/Search()?orderby=Id&$skip=0&$top=30&searchTerm='$1'&targetFramework=''&includePrerelease=false"

	echo "URL : $url"

	# XPath selection into the XML results and pick-up the latest version
	nuget_version=`curl $url | xmlstarlet sel -t -v '//d:Version' | sort -ur | head -n 1`
	
	echo "NuGet Version : $nuget_version"
}

#
# Increment version number
#
increment_version() {
	local current_version=$1

	new_version=$(($current_version + 1))
}

#
# Change the appropriate number of version
#
change_version() {
	local patch_version=`echo $1 | awk -F . '{print $3}'`
	local minor_version=`echo $1 | awk -F . '{print $2}'`
	local major_version=`echo $1 | awk -F . '{print $1}'`


	if [[ "$version_type" == "PATCH" ]]
	then
		increment_version $patch_version
		patch_version=$new_version

	elif [[ "$version_type" == "MINOR" ]]
	then
		increment_version $minor_version
		minor_version=$new_version

	elif [[ "$version_type" == "MAJOR" ]]
	then

		increment_version $major_version
		major_version=$new_version
	else
		echo "Nothing to do. Bye Bye ;-)"
	fi
	
	final="$major_version.$minor_version.$patch_version"
	
	echo "Final version is : $final"
	echo $final > $output_file
}

# Check if the branch name is null or empty
if [ -z "$version_type" ]
then
	# message
	echo
	echo " USAGE:"
	echo "  increase_version.sh PACKAGE_NAME REPOSITORY_URL VERSION_TYPE OUTPUT_FILE"
	echo 
	echo "  VERSION_TYPE can be one of the following values : (PATCH | MINOR | MAJOR)"
	echo

	# can exit easily
	exit 0
fi

#
# for future development, add a type flag and use it to create a if/else to choose how read and increase the version
# for now, only nuget

# call Nuget. 
nuget_read_version $package_name $repository_url

# unify variable
current_version=$nuget_version

if [ -z "$current_version" ];
then
	echo "Current version is NULL or EMPTY. Reset to 0.0.1 as DEFAULT VERSION"
	echo

	current_version="0.0.1"
fi

change_version $current_version

echo "That's all Folks !"

exit 0

