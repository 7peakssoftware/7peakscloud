FROM trion/ng-cli-karma:latest

# Environment variables
ENV NPM_CONFIG_LOGLEVEL="warn"
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true 
ENV CYPRESS_INSTALL_BINARY=0

# Node Pacakge manager
RUN npm install

# set the working directory
RUN mkdir /workDir
WORKDIR /workDir

# set the volume
VOLUME ["/workDir"]


