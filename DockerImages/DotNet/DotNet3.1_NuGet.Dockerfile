FROM mcr.microsoft.com/dotnet/core/sdk:3.1

# Create Jenkins user
RUN useradd -r -s /bin/false jenkins

# update system
RUN apt-get update \
	&& apt-get install -y apt-utils apt-transport-https curl wget gpg \ 
	&& apt-get upgrade -y \
	&& apt-get install -y nuget 

# set the working directory
RUN mkdir /workDir
WORKDIR /workDir

# set the volume
VOLUME ["/workDir"]