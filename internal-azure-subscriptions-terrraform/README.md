# INTERNAL AZURE RESOURCES #

This is the project to manage the internal azure subscription, in the specific :

 * Budget
 * Permissions

## Hot to use ##

In this steps there is the word **console**, it is referred to Window$ PowerShell or Linux Console .

Steps:

 1. Download latest Terraform version [here]()
 
 2. Install the Azure CLI [here]() or Upgrade the current installation [here]{}

 3. Open the console and by Azure CLI select the subscription that you want manage / update / delete

    1. Login as first by `az login`

    2. Check the list of subscriptions `az account list -o table`

    3. Select the target subscription  `az account set -s SUBSCRIPTION_ID`

    4. Double-check with the command of the point #2

 4. In the same console window, enter into the folder of the subscription that you want manage / update / delete

    1. Init the project on your local copy by `terraform init`

    3. Study the changes which are going to be applied by `terraform plan` ( please add `-refresh=true` to force the refresh the stored status with the current status of the resources )

    4. If all is correct execute `terraform apply`


