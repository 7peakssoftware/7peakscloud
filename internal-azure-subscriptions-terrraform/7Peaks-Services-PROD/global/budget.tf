// EARLY_WARNING_SPENDING_LIMIT
resource "azurerm_consumption_budget_subscription" "early_warning_spending_limit" {
  name            = "EARLY_WARNING_SPENDING_LIMIT"
  subscription_id = module.global.subscription_id

  amount     = 50
  time_grain = "BillingMonth"

  time_period {
    start_date = "2021-08-01T00:00:00Z"
    end_date   = "2052-08-01T00:00:00Z"
  }

  notification {
    enabled        = true
    operator       = "GreaterThanOrEqualTo"
    threshold      = 100
    contact_emails = ["server-alerts@7peakssoftware.com"]
  }
}

// HIGH_SPENDING_LIMIT
resource "azurerm_consumption_budget_subscription" "high_spending_limit" {
  name            = "HIGH_SPENDING_LIMIT"
  subscription_id = module.global.subscription_id

  amount     = 100
  time_grain = "BillingMonth"

  time_period {
    start_date = "2021-08-01T00:00:00Z"
    end_date   = "2052-08-01T00:00:00Z"
  }

  notification {
    enabled        = true
    operator       = "GreaterThanOrEqualTo"
    threshold      = 100
    contact_emails = ["server-alerts@7peakssoftware.com"]
  }
}

// CRITICAL_SPENDING_LIMIT
resource "azurerm_consumption_budget_subscription" "critical_spending_limit" {
  name            = "CRITICAL_SPENDING_LIMIT"
  subscription_id = module.global.subscription_id

  amount     = 200
  time_grain = "BillingMonth"

  time_period {
    start_date = "2021-08-01T00:00:00Z"
    end_date   = "2052-08-01T00:00:00Z"
  }

  notification {
    enabled        = true
    operator       = "GreaterThanOrEqualTo"
    threshold      = 100
    contact_emails = ["server-alerts@7peakssoftware.com"]
  }
}
