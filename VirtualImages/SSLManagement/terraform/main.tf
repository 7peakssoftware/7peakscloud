# Configure the Azure Provider
provider "azurerm" {
  version = "=2.14.0"
  features {}
}

terraform {
  backend "azurerm" {
    resource_group_name  = "7Peaks-WebSite"
    storage_account_name = "sevenpeakswebsitesa"
    container_name       = "7peaks-ssl-management"
    key                  = "7peaks-ssl-management.tfstate"
  }
}

# Create a resource group
#resource "azurerm_resource_group" "this" {
#  name     = var.resource_group_name
#  location = var.region_name
#}
