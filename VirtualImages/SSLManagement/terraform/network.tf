
resource "azurerm_virtual_network" "this" {
  name                = "${var.vnet_prefix_name}-vnet"
  address_space       = var.vnet_cidr
  location            = var.region_name
  resource_group_name = var.resource_group_name
}

resource "azurerm_subnet" "this" {
  name                 = "${var.vnet_prefix_name}-subnet"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.this.name
  address_prefixes     = var.vnet_cidr_subnet_01
}

resource "azurerm_network_interface" "this" {
  name                = "${var.instance_name}-nic"
  location            = var.region_name
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = "${var.instance_name}-ipconfig"
    subnet_id                     = azurerm_subnet.this.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.this.id
  }
}

resource "azurerm_public_ip" "this" {
  name                = "${var.instance_name}-ip"
  resource_group_name = var.resource_group_name
  location            = var.region_name
  allocation_method   = "Dynamic"
  domain_name_label   = "${var.instance_name}"
}
