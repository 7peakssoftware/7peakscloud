resource "azurerm_virtual_machine" "vm" {
  name                = var.instance_name
  location            = var.region_name
  resource_group_name = var.resource_group_name
  vm_size             = var.instance_class
  network_interface_ids = [
    azurerm_network_interface.this.id
  ]

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  # delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = var.instance_os_publisher
    offer     = var.instance_os_offer
    sku       = var.instance_os_sku
    version   = "latest"
  }

  os_profile {
    computer_name  = var.instance_name
    admin_username = var.instance_username
    custom_data    = file("${path.module}/userdata.sh.tpl")
  }

  storage_os_disk {
    name              = "${var.instance_name}-root"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
    disk_size_gb      = 32
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      key_data = file("keys/primary_key.pub")
      path     = "/home/${var.instance_username}/.ssh/authorized_keys"
    }
    ssh_keys {
      key_data = file("keys/secondary_key.pub")
      path     = "/home/${var.instance_username}/.ssh/authorized_keys"
    }
  }
}

resource "azurerm_network_security_group" "this" {
  name                = "${var.instance_name}-nsg"
  location            = var.region_name
  resource_group_name = var.resource_group_name

  security_rule {
    name                       = "SSH"
    priority                   = 300
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
