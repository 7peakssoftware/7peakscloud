#!/usr/bin/env bash
set -xe

# System set up
apt-get update 
apt-get install -y apt-utils
apt-get install -y wget curl vim less
apt-get install -y openssh-server
#mkdir /var/run/sshd

# Install CertBot - LetsEncrypt
apt-get install -y software-properties-common
add-apt-repository universe
apt-get update
apt-get install -y certbot

# Final assessment
systemctl restart sshd
