variable "resource_group_name" {
  type = string
}

variable "region_name" {
  type = string
}

variable "vnet_prefix_name" {
  type = string
}

variable "vnet_cidr" {
  type = list(string)
}

variable "vnet_cidr_subnet_01" {
  type = list(string)
}

variable "instance_name" {
  type = string
}

variable "instance_class" {
  type = string
}

variable "instance_os_publisher" {
  type = string
}

variable "instance_os_offer" {
  type = string
}

variable "instance_os_sku" {
  type = string
}

variable "instance_username" { 
  type = string 
}