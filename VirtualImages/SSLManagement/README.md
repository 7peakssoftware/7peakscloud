# Seven Peaks SSL Management #

The actual plan is the VM running under :

* Subscription : `7Peaks-Services-PROD`
* Resource Group : `7Peaks-WebSite`

The project includes also a solution for container, but `certbot` used by Letsencrypt are using the symlinks that are not compatible with Azure File Share.

## Rquirements ##

The usage and the operations are based on :

 * `certbot` tool - [Certbot Official WebSite](https://certbot.eff.org/)
 * DNS panel or management of the domain for the certificate to issue ( create or renew )

Until now (May 2021), the automation between certbot and DNS isn't created.

## Usage ##

### Connection ###

To connect in SSH should use the PRIMARY KEY placed into `terraform/keys` folder

```
 $> ssh -i terraform/keys/primary_key ssl-manager@ssl-management.southeastasia.cloudapp.azure.com
```

Maybe the level of the file for the primary key is `too open` and need to resolve this issue:

 * Linux, `chmod 400 <FILE_PATH>`
 * Windows, [procedure](https://superuser.com/questions/1296024/windows-ssh-permissions-for-private-key-are-too-open)

## Certbot Commands ##

Command sequence:

 1. Login as Root ( password is not required )

 ```
   $>sudo -s
 ```

 2. List the certificates

 ```
   $> certbot certificates
 ```

The output will some like that

```  
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Found the following certs:
...
    Certificate Name: findyourspace.co
        Serial Number: 4bf027628952790fb714ea57eeb4846e826
        Domains: *.findyourspace.co
        Expiry Date: 2021-05-17 12:43:36+00:00 (VALID: 5 days)
        Certificate Path: /etc/letsencrypt/live/findyourspace.co/fullchain.pem
        Private Key Path: /etc/letsencrypt/live/findyourspace.co/privkey.pem

    Certificate Name: property-flow.com
        Serial Number: 40117bcacc2ed3dff4020d3920840adbe97
        Domains: *.property-flow.com
        Expiry Date: 2021-05-17 12:45:21+00:00 (VALID: 5 days)
        Certificate Path: /etc/letsencrypt/live/property-flow.com/fullchain.pem
        Private Key Path: /etc/letsencrypt/live/property-flow.com/privkey.pem
```

 3. Create or renew the certificate

```
 $> certbot certonly --manual --agree-tos --preferred-challenges dns --email maintenance@7peakssoftware.com -d DOMAIN_NAME
```

Example for `DOMAIN_NAME` :

 - for `www.my_domain.com` use `www.my_domain.com`
 - for `*.property-flow.com` use `property-flow.com`
 

 4. Check the previous operation with the command of the point no. 2

```
   $> certbot certificates

    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Found the following certs:
...
    Certificate Name: property-flow.com
        Serial Number: 40117bcacc2ed3dff4020d3920840adbe97
        Domains: *.property-flow.com
        Expiry Date: YYYY-MM-DD HH:MM:SS+00:00 (VALID: XX days)    <------------- CHECK THIS VALUE
        Certificate Path: /etc/letsencrypt/live/property-flow.com/fullchain.pem
        Private Key Path: /etc/letsencrypt/live/property-flow.com/privkey.pem
...
```

 5. Execute the backup of new files

```
 $> rsync -va --exclude=live /etc/letsencrypt/* /mnt/sevenpeakswebsitesa/
```

 6. Convert the certificate from [PEM format](https://en.wikipedia.org/wiki/Privacy-Enhanced_Mail) to [PFX format](https://en.wikipedia.org/wiki/PKCS_12)

```
 $> openssl pkcs12 -export -out fullchain_certificate.pfx -inkey PATH/privkey.pem -in PATH/fullchain.pem

 Example:

 $> openssl pkcs12 -export \
    -out /mnt/sevenpeakswebsitesa/archive/property-flow.com/fullchain_cert.pfx \ 
    -inkey /etc/letsencrypt/live/property-flow.com/privkey.pem \
    -in /etc/letsencrypt/live/property-flow.com/fullchain.pem
```

**P.S.**: Normally the export password is `SevenPeaks` or `7Peaks`.

### Backup ###

The folder `/mnt/sevenpeakswebsitesa/` is the Azure File Share for backup of LetsEncrypt folder `/etc/letsencrypt`

To pick-up the new certificate:

* Go to the Azure Portal, under 7Peaks domain
* Go to `7Peaks-Services-PROD` subscription
* Go to `7Peaks-WebSite` Resource Group
* Select `sevenpeakswebsitesa` storage account
* Select `Azure File Share`
* Go into the folder `ssl-management`

## Terraform ##

Terraform templates are build to create infrastructure.

Sequence of command to create or modify it:

 1. Login into Azure : 
 
```
 $> az login
```

 2. Set to the account and check the configuration

```
 $> az account set -s c03da5c0-c42d-494c-a81c-e0586a1e6697

 $> az account list -o table

 Name                         CloudName    SubscriptionId                        State    IsDefault
 ---------------------------  -----------  ------------------------------------  -------  -----------
 Playground                   AzureCloud   66402a05-8d6d-4ccc-ad6a-378e19cc79ea  Enabled  False
 7Peaks-BuildServer-PROD      AzureCloud   6b32ace3-2b1c-48c3-979f-a8ed19f627e6  Enabled  False
 7Peaks-Services-PROD         AzureCloud   c03da5c0-c42d-494c-a81c-e0586a1e6697  Enabled  True   <------- Subscription to set
 7Peaks-Internal-PROD         AzureCloud   17feac5f-4e6a-4578-a684-b674da2525a9  Enabled  False
```
 
 If `IsDefault` is set to `True` , then the configuration is done correctly.

 3. Execute `terraform init` to download all the required modules.

 4. Execute `terraform plan` and `terraform apply` to check the change and after apply them.