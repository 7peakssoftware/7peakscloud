# ProGet #

This is the folder to manage the Virtual Machine for Internal ProGet repository.

## Details ##

Coordinates:

| Property Name | Property Value |
|---------------|----------------|
| Subscription | `7Peaks-BuildServer-PROD` |
| Resource Group | `7Peaks-ProGet` |
| VM name | `proget-prod-proget` |

Credentials of the VM and Proget is managed by [Giorgio Desideri](mailto:giorgio@7peakssoftware.com)