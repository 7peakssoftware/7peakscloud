// Outputs
output "region" {
  value = var.region
}

output "web_application_remote_state_key" {
  value = "${var.region}-web-application"
}

output "database_remote_state_key" {
  value = "${var.region}-database"
}

output "api_remote_state_key" {
  value = "${var.region}-api-management"
}
