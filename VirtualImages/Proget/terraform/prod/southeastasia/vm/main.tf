# Configure the Azure Provider
provider "azurerm" {
  version = "=2.8.0"
  features {}
}

module "global" {
  source = "../../global-vars"
}

module "region" {
  source = "../region-vars"
}

module "vm_net" {
  source = "git::https://bitbucket.org/7peakssoftware/7peaks-terraform-library.git//az-vnet?ref=develop"

  environment_name    = module.global.environment_name
  project_name        = module.global.project_name
  resource_group_name = module.global.resource_group_name
  region              = module.region.region

  vnet_name = "proget"
  vnet_cidr = ["192.168.23.0/24"]
  vnet_subnets = {
    "subnet01" = "192.168.23.0/26"
    "subnet02" = "192.168.23.64/26"
    "subnet03" = "192.168.23.128/26"
    "subnet04" = "192.168.23.192/26"
  }
}

module "vm_instance" {
  source = "git::https://bitbucket.org/7peakssoftware/7peaks-terraform-library.git//az-virtual-machine?ref=develop"

  environment_name    = module.global.environment_name
  project_name        = module.global.project_name
  resource_group_name = module.global.resource_group_name
  region              = module.region.region

  domain_vm_name = "sevenpeaks-proget"
  vm_subnet_id   = module.vm_net.vnet_subnet_ids[0]

  vm_windows = true

  vm_name                    = "proget"
  vm_computer_name           = "pc"
  vm_computer_admin_username = "7peaks-proget"

  vm_size         = "Standard_B2ms"
  vm_os_disk_size = 150
  //vm_os_disk_caching = "ReadWrite"
  //vm_os_disk_storage_account_type = "Standard_LRS"

  vm_image_publisher = "MicrosoftWindowsServer"
  vm_image_offer     = "WindowsServer"
  vm_image_sku       = "2019-Datacenter"
  vm_image_version   = "latest"

  custom_data_path = "custom-data.ps1"
}

resource "azurerm_storage_account" "storage_account" {
  name                     = "7peaksprogetprodsa"
  resource_group_name      = module.global.resource_group_name
  location                 = module.global.default_region
  account_tier             = "Standard"
  account_replication_type = "ZRS"
}