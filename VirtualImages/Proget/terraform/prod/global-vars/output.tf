output "account_id" {
  value = ""
}

output "default_region" {
  value = "southeastasia"
}

output "customer_name" {
  value = "7peaks"
}

output "project_name" {
  value = "proget"
}

output "environment_name" {
  value = "prod"
}

output "resource_group_name" {
  value = "7Peaks-ProGet"
}