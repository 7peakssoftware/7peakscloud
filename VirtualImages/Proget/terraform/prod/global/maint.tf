# Configure the Azure Provider
provider "azurerm" {
  version = "=2.8.0"
  features {}
}

module "global" {
  source = "../global-vars"
}