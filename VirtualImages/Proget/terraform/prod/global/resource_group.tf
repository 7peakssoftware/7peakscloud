# Create a resource group
resource "azurerm_resource_group" "this" {
  name     = module.global.resource_group_name
  location = module.global.default_region
}