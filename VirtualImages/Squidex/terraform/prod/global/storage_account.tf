module "storage_account" {
  source = "git::https://bitbucket.org/7peakssoftware/7peaks-terraform-library.git//az-storage-account?ref=develop"

  resource_group_name = module.global.resource_group_name
  
  name = join("", [module.global.project_name, module.global.environment_name, "sa"])
  tier = "Standard"
  replication = "ZRS"
}

resource "time_sleep" "wait_10_seconds" {
  depends_on = [ module.storage_account ]

  create_duration = "10s"
}

resource "azurerm_storage_share" "file_share_001" {
  name                 = "mongodb-data-store"
  storage_account_name = join("", [module.global.project_name, module.global.environment_name, "sa"])
  quota                = 50

  depends_on = [time_sleep.wait_10_seconds]
}