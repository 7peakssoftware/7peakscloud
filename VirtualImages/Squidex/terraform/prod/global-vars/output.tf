output "default_region" {
  value = "southeastasia"
}

output "customer_name" {
  value = "SevenPeaks"
}

output "project_name" {
  value = "squidex"
}

output "environment_name" {
  value = "prod"
}

output "resource_group_name" {
  value = "squidex-rg"
}