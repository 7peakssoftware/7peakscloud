# Configure the Azure Provider
provider "azurerm" {
  version = "=2.26.0"
  features {}
}

module "global" {
  source = "../../global-vars"
}

module "region" {
  source = "../region-vars"
}

