
module "squiddex" {
  source = "git::https://bitbucket.org/7peakssoftware/7peaks-terraform-library.git//az-webapp-container?ref=develop"

  project_name        = module.global.project_name
  environment_name    = module.global.environment_name
  region              = module.region.region
  resource_group_name = module.global.resource_group_name

  app_service_plan_kind     = "Linux"
  app_service_plan_reserved = true
  sku_tier                  = "Basic"
  sku_size                  = "B1"

  web_app_name = "${module.global.project_name}-${module.global.environment_name}-webapp"

  web_app_https_only      = true
  web_app_arr_affinity    = true
  docker_image_repo       = "DOCKER|squidex/squidex:4.6.0"
  docker_app_command_line = ""
  web_app_http2           = true
  web_app_ip_restrictions = []
  web_app_cors            = [{ allowed_origins = ["*"], support_credentials = false }]
  web_app_settings = {
    "ASSETSTORE__AZUREBLOB__CONNECTIONSTRING" = "DefaultEndpointsProtocol=https;AccountName=squidexprodsa;AccountKey=txAvXrkoxFfDFLUo1Bx6xu7lBCkv/U4IV+Z3rMmSs3H64kobav/52t4WLPvBoAhbDN/bGyjJ+HlPDFC2NH/itg==;EndpointSuffix=core.windows.net",
    "ASSETSTORE__AZUREBLOB__CONTAINERNAME"    = "etc-squidex-assets",
    "ASSETSTORE__TYPE"                        = "AzureBlob",
    "EVENTSTORE__MONGODB__CONFIGURATION"      = "mongodb://SquidexAdminUser:HHT%40X%21C59W%259RYxZKBvBUpcA%263hFpgDFak%40hzJuvDQKZ6X%2BkxWUPyhqLpBFc%26c%26%24@52.163.208.92:27017",
    "STORE__MONGODB__CONFIGURATION"           = "mongodb://SquidexAdminUser:HHT%40X%21C59W%259RYxZKBvBUpcA%263hFpgDFak%40hzJuvDQKZ6X%2BkxWUPyhqLpBFc%26c%26%24@52.163.208.92:27017",
    "IDENTITY__ADMINEMAIL"                    = "",
    "IDENTITY__ADMINPASSWORD"                 = "",
    "IDENTITY__GITHUBCLIENT"                  = "",
    "IDENTITY__GITHUBSECRET"                  = "",
    "IDENTITY__GOOGLECLIENT"                  = "",
    "IDENTITY__GOOGLESECRET"                  = "",
    "IDENTITY__MICROSOFTCLIENT"               = "a57b3df9-1121-415d-8239-ecc9a6946908",
    "IDENTITY__MICROSOFTSECRET"               = "7Z-4Bhgs~6SVD73cgkdVDNn__3CWLW3~pg",
    "URLS__BASEURL"                           = ""
  }
  web_app_connection_strings = {}
  web_app_log_level          = "Information"
  web_app_log_retention_days = 2

  web_app_log_blob_storage_url = "https://squidexprodsa.blob.core.windows.net/squidex-diagnostic?sv=2019-12-12&ss=b&srt=co&sp=rwdlacx&se=2020-11-29T18:26:35Z&st=2020-09-06T10:26:35Z&spr=https&sig=ilfWuD%2BfPHVtzXJ4YIi%2Fgt1zrqiBZFAr4hNTk%2Fjzbcc%3D"
}

# MongoDb password for Squidex
#
# user: "SquidexAdminUser" 
# password: "HHT@X!C59W%9RYxZKBvBUpcA&3hFpgDFak@hzJuvDQKZ6X+kxWUPyhqLpBFc&c&$"
#
