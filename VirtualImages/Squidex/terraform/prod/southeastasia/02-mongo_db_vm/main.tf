# Configure the Azure Provider
provider "azurerm" {
  version = "=2.26.0"
  features {}
}

module "global" {
  source = "../../global-vars"
}

module "region" {
  source = "../region-vars"
}

data "terraform_remote_state" "vnet" {
  backend = "azurerm"
  config = {
    resource_group_name  = "7Peaks-CICD-Commons"
    storage_account_name = "sevenpeakscistorage"
    container_name       = "sevenpeaks-squidex-tfstate"
    key                  = "vnet"
    access_key           = "hgn5qQsYo1rb1euahIjYBUx0W10+QUVV1o5SR38TV3jyaFYdmL0YN0D9oRKO09FNir4R4cyBXh6bOC0+AGCsUA=="
  }
}