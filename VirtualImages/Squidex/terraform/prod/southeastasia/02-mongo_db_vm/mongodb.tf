module "vm_instance" {
  source = "git::https://bitbucket.org/7peakssoftware/7peaks-terraform-library.git//az-virtual-machine?ref=develop"

  environment_name    = module.global.environment_name
  project_name        = module.global.project_name
  resource_group_name = module.global.resource_group_name
  region              = module.region.region

  domain_vm_name = "sevenpeaks-squidex-prod"
  vm_subnet_id   = data.terraform_remote_state.vnet.outputs.vsubnet_ids[0]

  vm_windows = false

  vm_name                    = "mongodb"
  vm_computer_name           = "mongodb"
  vm_computer_admin_username = "sevenpeaks-admin"

  public_key_path  = "${path.module}/keys/id_key.pub"
  custom_data_path = "${path.module}/azure-user-data.sh"

  vm_size = "Standard_B1ms"

  vm_os_disk_size                 = 64
  vm_os_disk_caching              = "ReadWrite"
  vm_os_disk_storage_account_type = "StandardSSD_LRS"

  vm_image_publisher = "Canonical"
  vm_image_offer     = "UbuntuServer"
  vm_image_sku       = "18.04-LTS"
  vm_image_version   = "latest"
}

# Keep public due to prepare the bastion host
# resource "azurerm_network_interface_security_group_association" "nsg_nic_association" {
#  network_interface_id      = module.vm_instance.vm_network_interface_id
#  network_security_group_id = data.terraform_remote_state.vnet.outputs.nsg_id_001
# }

