#!/bin/bash

# Prepare System
apt-get update 
apt-get upgrade -y
apt-get install -y apt-utils gnupg vim tee openssl

# Mount azure share files
if [ ! -d "/etc/smbcredentials" ]; then
sudo mkdir /etc/smbcredentials
fi
if [ ! -f "/etc/smbcredentials/sevenpeakscistorage.cred" ]; then
    sudo bash -c 'echo "username=squidexprodsa" >> /etc/smbcredentials/squidexprodsa.cred'
    sudo bash -c 'echo "password=" >> /etc/smbcredentials/squidexprodsa.cred'
fi
sudo chmod 600 /etc/smbcredentials/sevenpeakscistorage.cred

bash -c 'echo "//squidexprodsa.file.core.windows.net/mongodb-data-store /var/lib/mongodb cifs nofail,vers=3.0,credentials=/etc/smbcredentials/squidexprodsa.cred,uid=1000,gid=1000,dir_mode=0777,file_mode=0777,serverino" >> /etc/fstab'
mount -t cifs //squidexprodsa.file.core.windows.net/mongodb-data-store /var/lib/mongodb -o vers=3.0,credentials=/etc/smbcredentials/squidexprodsa.cred,uid=1000,gid=1000,dir_mode=0777,file_mode=0777,serverino
mount -v

# Install MongoDB Community
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
apt-get update
apt-get install -y mongodb-org

# Stop Upgrade of MongoDB - Remove them if you want to upgrade
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-org-shell hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections

# AUtomatic start mongodb
systemctl enable mongod.service
systemctl restart mongod.service

#
# Mongo DB configuration
#
ulimit -n 21000

# Create Admin user
cat > /root/script_admin.js << EOF
use admin
db.createUser(
  {
    user: "SevenPeaksMongoDbAdmin",
    pwd: "z4eSg$kQ7K322d@q=n9HrK%Y82e8T+q6NYhD9W9Fp+QsUUpWN7FvBw5K$Yhj-EUV",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
  }
)

db.adminCommand( { shutdown: 1 } )
EOF

mongo /root/script_admin.js

# Override the configuration file now to restart MongoDB with Authentication !!!!
cat > /etc/mongod.conf << EOF 
# mongod.conf

# Where and how to store data.
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
#  engine:
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1

# how the process runs
processManagement:
  timeZoneInfo: /usr/share/zoneinfo

# security
security:
  authorization: enabled

#operationProfiling:

#replication:

#sharding:
EOF

# Relax everything
systemctl stop mongod.service

chown -R mongodb:mongodb /var/lib/mongodb
chown mongodb:mongodb /tmp/mongodb-27017.sock
chown mongodb:mongodb /etc/mongod.conf

# Rest Root Password
echo "root:.P@ssw0rd1!"|chpasswd
