output "vsubnet_ids" {
    value = module.vm_net.vnet_subnet_ids
}

output "nsg_id_001" {
    value = azurerm_network_security_group.nsg_mongodb_nic.id
}