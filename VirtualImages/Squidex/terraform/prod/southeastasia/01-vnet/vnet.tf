module "vm_net" {
  source = "git::https://bitbucket.org/7peakssoftware/7peaks-terraform-library.git//az-vnet?ref=develop"

  environment_name    = module.global.environment_name
  project_name        = module.global.project_name
  resource_group_name = module.global.resource_group_name
  region              = module.region.region

  vnet_name = "squidex-rg-vnet"
  vnet_cidr = ["192.168.23.0/24"]
  vnet_subnets = {
    "subnet01" = "192.168.23.0/26"
    "subnet02" = "192.168.23.64/26"
    "subnet03" = "192.168.23.128/26"
    "subnet04" = "192.168.23.192/26"
  }
}

#
# Network Security Group
# 
resource "azurerm_network_security_group" "nsg_mongodb_nic" {
  name                = "${module.global.project_name}-${module.global.environment_name}-nsg-mongodb"
  location            = module.region.region
  resource_group_name = module.global.resource_group_name
}

#
# SSH Access
#
resource "azurerm_network_security_rule" "ssh_access" {
  resource_group_name         = module.global.resource_group_name
  network_security_group_name = azurerm_network_security_group.nsg_mongodb_nic.name
  name                        = "ssh-access-rule"
  priority                    = 200
  direction                   = "Inbound"
  access                      = "Allow"
  source_address_prefix       = "*"
  source_port_range           = "22"
  destination_address_prefix  = "192.168.23.0/24"
  destination_port_range      = "22"
  protocol                    = "TCP"
}

#
# MongoDB Access
#
resource "azurerm_network_security_rule" "mongodb_access" {
  resource_group_name         = module.global.resource_group_name
  network_security_group_name = azurerm_network_security_group.nsg_mongodb_nic.name
  name                        = "mongodb-access-rule"
  priority                    = 201
  direction                   = "Inbound"
  access                      = "Allow"
  source_address_prefix       = "192.168.23.0/24"
  source_port_range           = "27017-27019"
  destination_address_prefix  = "192.168.23.0/24"
  destination_port_range      = "27017-27019"
  protocol                    = "TCP"
}