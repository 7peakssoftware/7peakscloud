# SevenPeak Squidex Instance #

Squidex stack is based upon MongoDB instance and WebApplication as Container.

## Mongo DB VM ##

**IMPORTANT : This configuration is a single VM, to scale up need to create a VMSS with the custom data to create and install a MongoDB Cluster**

Information details

| Property Name  | Property Value  |
|----------------|-----------------|
| Subscription   | `7Peaks-Internal-PROD`  |
| Resource Group | `squidex-rg`  |
| OS             | Ubuntu 20.04 |
| MongoDB        | Community v. 4.4 |

No shared the Administrator credentials due to follow the right approach: **Login as Administrator to create another user with the right permissions**.

### Connection ###

`ssh -i terraform\prod\southeastasia\02-mongo_db_vm\keys\id_key sevenpeaks-admin@sevenpeaks-squidex-prod.southeastasia.cloudapp.azure.com`

### Create a New User ###

Follow the sequence of the commands to create the new user.

 1. Connect to MongoDB as Admin

   `mongo --shell --port 27017 -u SevenPeaksMongoDbAdmin  --authenticationDatabase 'admin' -p`

 2. Create the new user

```
use admin

db.createUser( { 
   user: "MY_NEW_USER",
   pwd: "MY_NEW_USER_PASSWORD",
   customData: { 
      project_name: "PROJECT_NAME"  
   },
   roles: [ { role: "readWrite", db: "NEW_DATABASE" } ] } )
   ```

 3. Test connection with new user
   
    `mongo --shell --port 27017 -u MY_NEW_USER --authenticationDatabase 'admin' -p `

 4. Enjoy it