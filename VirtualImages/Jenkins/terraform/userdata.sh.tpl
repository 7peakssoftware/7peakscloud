#!/usr/bin/env bash
set -xe

# Install Jenkins and nginx
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
echo "deb https://pkg.jenkins.io/debian-stable binary/" >> /etc/apt/sources.list
apt-get update
apt-get -y install default-jdk cifs-utils

sudo mkdir -p /var/lib/jenkins
if [ ! -d "/etc/smbcredentials" ]; then
sudo mkdir /etc/smbcredentials
fi
if [ ! -f "/etc/smbcredentials/sevenpeakscistorage.cred" ]; then
    sudo bash -c 'echo "username=sevenpeakscistorage" >> /etc/smbcredentials/sevenpeakscistorage.cred'
    sudo bash -c 'echo "password=hgn5qQsYo1rb1euahIjYBUx0W10+QUVV1o5SR38TV3jyaFYdmL0YN0D9oRKO09FNir4R4cyBXh6bOC0+AGCsUA==" >> /etc/smbcredentials/sevenpeakscistorage.cred'
fi
sudo chmod 600 /etc/smbcredentials/sevenpeakscistorage.cred

bash -c 'echo "//sevenpeakscistorage.file.core.windows.net/jenkins-home /var/lib/jenkins cifs nofail,vers=3.0,credentials=/etc/smbcredentials/sevenpeakscistorage.cred,uid=1000,gid=1000,dir_mode=0777,file_mode=0777,serverino" >> /etc/fstab'
mount -t cifs //sevenpeakscistorage.file.core.windows.net/jenkins-home /var/lib/jenkins -o vers=3.0,credentials=/etc/smbcredentials/sevenpeakscistorage.cred,uid=1000,gid=1000,dir_mode=0777,file_mode=0777,serverino
mount -v

apt-get -y install jenkins nginx

openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -subj "/C=TH/ST=Thailand/L=Bangkok/O=7Peaks Software/CN=sevenpeaks-cicd.southeastasia.cloudapp.azure.com" -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt
openssl dhparam -out /etc/nginx/dhparam.pem 4096

cat > /etc/nginx/snippets/self-signed.conf << EOF
ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;
EOF

cat > /etc/nginx/snippets/ssl-params.conf << EOF
ssl_protocols TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_dhparam /etc/nginx/dhparam.pem;
ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
ssl_session_timeout  10m;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off; # Requires nginx >= 1.5.9
ssl_stapling on; # Requires nginx >= 1.3.7
ssl_stapling_verify on; # Requires nginx => 1.3.7
resolver 8.8.8.8 8.8.4.4 valid=300s;
resolver_timeout 5s;
# Disable strict transport security for now. You can uncomment the following
# line if you understand the implications.
# add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;
add_header X-XSS-Protection "1; mode=block";
EOF

# Configure nginx
cat > /etc/nginx/sites-available/default << EOF
server {

    listen 443 ssl;
    include snippets/self-signed.conf;
    include snippets/ssl-params.conf;

    server_name sevenpeaks-cicd.southeastasia.cloudapp.azure.com;

    location / {
        proxy_set_header        Host \$host:\$server_port;
        proxy_set_header        X-Real-IP \$remote_addr;
        proxy_set_header        X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto \$scheme;

        proxy_pass http://localhost:8080;
        proxy_redirect http://localhost:8080 http://sevenpeaks-cicd.southeastasia.cloudapp.azure.com;
    }
}
EOF

systemctl restart nginx

# Docker
apt-get -y install apt-transport-https \
  ca-certificates \
  curl \
  gnupg-agent \
  software-properties-common
wget -q -O - https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt-get update
apt-get -y install docker-ce docker-ce-cli containerd.io

# Allow Jenkins to run docker commands
usermod -a -G docker jenkins
newgrp docker

systemctl restart docker
systemctl restart jenkins
