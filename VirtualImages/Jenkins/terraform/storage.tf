
resource "azurerm_storage_container" "terraform_state" {
  name                  = "7peaks-cicd-tfstate"
  storage_account_name  = "sevenpeakscistorage"
  container_access_type = "private"
}
