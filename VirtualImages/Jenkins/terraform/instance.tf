resource "azurerm_virtual_machine" "jenkins" {
  name                = "7peaks-jenkins"
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
  vm_size             = "Standard_B2s"
  network_interface_ids = [
    azurerm_network_interface.this.id
  ]

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  # delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "jenkins"
    admin_username = "jenkins"
    custom_data    = file("${path.module}/userdata.sh.tpl")
  }

  storage_os_disk {
    name              = "7peaks-jenkins-root"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      key_data = file("keys/id_rsa_jenkins.pub")
      path     = "/home/jenkins/.ssh/authorized_keys"
    }
    ssh_keys {
      key_data = file("keys/id_rsa_jenkins_backup.pub")
      path     = "/home/jenkins/.ssh/authorized_keys"
    }
  }
}

resource "azurerm_network_security_group" "this" {
  name                = "7peaks-jenkins-nsg"
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name

  security_rule {
    name                       = "SSH"
    priority                   = 300
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "HTTPS"
    priority                   = 320
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "Jenkins_TCP_port_for_inbound_agents"
    priority                   = 350
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "50000"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
