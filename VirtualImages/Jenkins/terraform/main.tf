# Configure the Azure Provider
provider "azurerm" {
  version = "=2.12.0"
  features {}
}

terraform {
  backend "azurerm" {
    resource_group_name  = "7Peaks-CICD-Commons"
    storage_account_name = "sevenpeakscistorage"
    container_name       = "7peaks-cicd-tfstate"
    key                  = "terraform-state-prod.tfstate"
  }
}

# Create a resource group
resource "azurerm_resource_group" "this" {
  name     = "7Peaks-CICD"
  location = "Southeast Asia"
}
