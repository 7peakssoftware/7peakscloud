FROM jenkins/jenkins:2.237

ENV JENKINS_SLAVE_AGENT_PORT 50001

EXPOSE 8080
EXPOSE 2222 

# if we want to install via apt
USER root
RUN apt-get update && apt-get install -y apt-utils openssl ca-certificates sudo

# Dialog installation
RUN apt-get install -y --no-install-recommends dialog 

# SSH configuration
RUN apt-get install -y --no-install-recommends openssh-server \
  && echo "root:Docker!" | chpasswd

COPY Jenkins/sshd_config /etc/ssh/sshd_config
RUN service ssh restart

# Jenkins user as sudoers
RUN echo "jenkins ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/jenkins && \
    chmod 0440 /etc/sudoers.d/jenkins

# drop back to the regular jenkins user - good practice
USER jenkins

# Run configuration
CMD sudo /etc/init.d/ssh start ; /sbin/tini -- /usr/local/bin/jenkins.sh