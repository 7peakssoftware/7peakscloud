Param(
  [Parameter(Mandatory=$True)]
  [string]$GitUser,
  
  [Parameter(Mandatory=$False)]
  [string]$BeBranch = "master",
  
  [Parameter(Mandatory=$False)]
  [string]$FeBranch = "master",
  
  [Parameter(Mandatory=$False)]
  [string]$BackEndDirectory = "./irpc-dip-api",
  
  [Parameter(Mandatory=$False)]
  [string]$FrontEndDirectory = "./irpc-dashboard-frontend"
)

function GitExecute($GitParams){
	# Get GIT installation
	$GitPath=& "C:\Windows\System32\where.exe" git
	if($GitPath.Count -gt 1)
	{
		$GitPath=$GitPath[0]
	}

	Start-Process $GitPath -ArgumentList $GitParams -NoNewWindow -PassThru -Wait;
}

#
# Perform the git clone -b $BranchName $GitUrl $TargetDir
#
function GitCloneCode([string]$GitUrl, [string]$BranchName, [string]$TargetDir, [bool]$SubModule) {
	$GitCloneParams = @(
		"clone"
		"-b $BranchName"
		"$GitUrl"
		"$TargetDir"
	);
	
	Write-Output "Clone Project, Branch name $BranchName into $TargetDir ... ";
	GitExecute $GitCloneParams;
	Write-Output "Clone Project, Branch name $BranchName into $TargetDir ... OK";
	
	if($SubModule) {
		Write-Output "Submodules ...";
		Set-Location -Path $TargetDir;
		$GitSubModuleParams = @(
			"submodule"
			"update"
			"--init"
			"--recursive"
			"--remote"
		);
		
		GitExecute $GitSubModuleParams;
		Write-Output "Submodules ... OK";
	}
}

#
# Perform Git PULL
#
function GitUpdateCode([string]$BranchName, [string]$TargetDir, [bool]$SubModule) {
	Set-Location -Path $TargetDir
	
	# Checkout First
	$GitArguments = @(
		"checkout"
		"$BranchName"
	);
	Write-Output "Checkout branch $BranchName ...";
	GitExecute $GitArguments;
	Write-Output "Checkout branch $BranchName ... OK";
	
	# Pull
	$GitArguments = @(
		"pull"
	);
	
	Write-Output "Pull branch $BranchName ...";
	GitExecute $GitArguments;
	Write-Output "Pull branch $BranchName ... OK";
	
	if($SubModule) {
		Write-Output "Submodules ...";
		$GitSubModuleParams = @(
			"submodule"
			"update"
			"--remote"
		);
		
		GitExecute $GitSubModuleParams;
		Write-Output "Submodules ... OK";
	}
}

# Set in the same path of the script
$CurrentDirectory = (Get-Item -Path ".\").FullName;
Set-Location -Path $CurrentDirectory;

#
# Back-End
#
if((Test-Path $BackEndDirectory) -ne $true)
{
	GitCloneCode "https://$GitUser@bitbucket.org/7peakssoftware/irpc-dip-api.git" $BeBranch $BackEndDirectory $True;	
} else {
	GitUpdateCode $BeBranch $BackEndDirectory $False;
}

# Reset location at the exit
Set-Location -Path $CurrentDirectory;

#
# Front-End
#
if((Test-Path $FrontEndDirectory) -ne $true)
{
	GitCloneCode "https://$GitUser@bitbucket.org/7peakssoftware/irpc-dashboard-frontend.git" $FeBranch $FrontEndDirectory $False;	
} else {
	GitUpdateCode $FeBranch $FrontEndDirectory $False;
}

# Reset location at the exit
Set-Location -Path $CurrentDirectory;