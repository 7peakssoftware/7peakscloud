export const environment = {
  production: false,
  serviceWorker: false,
  hmr: true,
  adalConfigs: {
    tenant: 'afe1a651-91f4-4bc5-bfd0-a7a17628ae1a',
    clientId: 'e344ae7e-f7cc-48df-9712-95b1d9422690',
    redirectUri: 'http://localhost:4300/auth/callback',
    postLogoutRedirectUri: 'http://localhost:4300/auth/login',
    navigateToLoginRequestUrl: false,
    cacheLocation: 'localStorage'
  },
  apiBaseUrl: 'v1',
  appLandingUrl: '/app'
};
