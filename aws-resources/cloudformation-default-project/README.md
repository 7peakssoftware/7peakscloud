# AWS - CloudFormation Default Project #

This is the default project for the cloud formation templates.

As 'default project' it means the default structure and common that usually is going to use. Every project is different, so can exist some deviation and customization according to the project requirements.

# Useful links

AWS as default support [CDK](https://aws.amazon.com/cdk/). Cloud formation templates are often requested by the customer, also if the standard and easy template amangement is based on Terraform.

Useful links:

* [AWS Best Practices](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/best-practices.html)

* [7Peaks AWS Guidelines Wiki](https://bitbucket.org/7peakssoftware/wiki/wiki/SoftwareDevelopment/aws/aws-guidelines-index)