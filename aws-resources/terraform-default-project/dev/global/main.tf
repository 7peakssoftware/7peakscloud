provider "aws" {
  region              = module.global.default_region
  allowed_account_ids = [module.global.account_id]
  version             = "~>2.0"
}

module "global" {
  source = "../global-vars"
}