resource "aws_s3_bucket" "default" {
  bucket = module.global.terraform_state_bucket

  versioning {
    enabled = true
  }
}
