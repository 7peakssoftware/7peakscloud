output "account_id" {
  value = "412361854104"
}

output "default_region" {
  value = "ap-southeast-2"
}

output "customer_name" {
  value = "cloud-team"
}

output "project_name" {
  value = "terraform"
}

output "environment_name" {
  value = "dev"
}

output "terraform_state_bucket" {
  value = "cloud-team-terraform-dev-tfstate"
}