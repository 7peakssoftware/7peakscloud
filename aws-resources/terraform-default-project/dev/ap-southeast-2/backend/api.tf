resource "aws_s3_bucket" "default" {
  bucket = "${module.global.customer_name}-${module.global.project_name}-api-beanstalk-${module.global.environment_name}"
}

resource "aws_s3_bucket_object" "default" {
  bucket = aws_s3_bucket.default.id
  key    = "nodejs-v1.zip"
  source = "app/nodejs-v1.zip"
  etag   = filemd5("app/nodejs-v1.zip")
}

module "backend" {
  source = "git@bitbucket.org:7peakssoftware/7peaks-terraform-library.git//backend-beanstalk?ref=develop"

  customer_name        = module.global.customer_name
  environment_name     = module.global.environment_name
  project_name         = module.global.project_name
  aws_region           = module.region.region
  application          = "api"
  application_subnets  = data.terraform_remote_state.networking.outputs.private_subnet_ids
  loadbalancer_subnets = data.terraform_remote_state.networking.outputs.public_subnet_ids
  vpc_id               = data.terraform_remote_state.networking.outputs.vpc_id
  bucket_id            = aws_s3_bucket.default.id
  bucket_object_id     = aws_s3_bucket_object.default.id
  stack_name           = "64bit Amazon Linux 2 v5.0.1 running Node.js 12"
  project_description  = "My cloud team project"
  healthcheck_url      = "/"
  version_name         = "dev-1"
  env_vars = {
    ASPNETCORE_ENVIRONMENT = "Development"
  }

}
