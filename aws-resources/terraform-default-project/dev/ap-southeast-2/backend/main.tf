provider "aws" {
  region              = module.global.default_region
  allowed_account_ids = [module.global.account_id]
  version             = "~>2.0"
}

data "terraform_remote_state" "networking" {
  backend = "s3"

  config = {
    bucket  = module.global.terraform_state_bucket
    region  = module.global.default_region
    key     = "ap-southeast-2-networking"
    encrypt = true
  }
}

module "global" {
  source = "../../global-vars"
}

module "region" {
  source = "../region-vars"
}