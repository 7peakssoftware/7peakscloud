module "networking" {
  source = "git@bitbucket.org:7peakssoftware/7peaks-terraform-library.git//networking?ref=develop"

  customer_name      = module.global.customer_name
  environment_name   = module.global.environment_name
  project_name       = module.global.project_name
  cidr               = module.region.vpc_cidr
  public_subnets     = module.region.public_subnet_cidrs
  private_subnets    = module.region.private_subnet_cidrs
  availability_zones = module.region.availability_zones

}