variable "region" {
  default = "ap-southeast-2"
}

output "region" {
  value = var.region
}

output "private_subnet_cidrs" {
  value = ["192.168.23.96/27", "192.168.23.128/27", "192.168.23.160/27"]
}

output "public_subnet_cidrs" {
  value = ["192.168.23.0/27", "192.168.23.32/27", "192.168.23.64/27"]
}

output "availability_zones" {
  value = ["${var.region}a", "${var.region}b", "${var.region}c"]
}

output "vpc_cidr" {
  value = "192.168.23.0/24"
}
