terraform {
  required_version = "= 0.12.25"

  backend "s3" {
    bucket = "cloud-team-terraform-sandbox-tfstate"
    key    = "ap-southeast-2-frontend"
    region = "ap-southeast-2"
  }
}
