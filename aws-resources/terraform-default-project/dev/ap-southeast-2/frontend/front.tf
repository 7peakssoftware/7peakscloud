module "frontend" {
  source = "git@bitbucket.org:7peakssoftware/7peaks-terraform-library.git//frontend?ref=develop"

  customer_name    = module.global.customer_name
  environment_name = module.global.environment_name
  project_name     = module.global.project_name
  application      = "web"
  aws_region       = module.region.region
  max_ttl          = 0
  min_ttl          = 0
  default_ttl      = 0
}