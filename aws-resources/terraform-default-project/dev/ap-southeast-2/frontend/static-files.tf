resource "aws_s3_bucket_object" "static-files" {
  bucket       = module.frontend.bucket_id
  key          = "index.html"
  source       = "static-files/index.html"
  content_type = "text/html"
  etag         = filemd5("static-files/index.html")
}

resource "aws_s3_bucket_object" "static-files-404" {
  bucket       = module.frontend.bucket_id
  key          = "404.html"
  source       = "static-files/404.html"
  content_type = "text/html"
  etag         = filemd5("static-files/404.html")
}